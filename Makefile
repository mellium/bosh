.PHONEY: test
test:
	go test -cover $$(go list ./... | grep -v '/vendor/')

.PHONEY: bench
bench:
	go test -cover -bench . -benchmem -run 'Benchmark.*' ./...

deps.svg: *.go
	(   echo "digraph G {"; \
	go list -f '{{range .Imports}}{{printf "\t%q -> %q;\n" $$.ImportPath .}}{{end}}' \
		$$(go list -f '{{join .Deps " "}}' .) .; \
	echo "}"; \
	) | dot -Tsvg -o $@
