// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"context"
)

// The ctxkey type is unexported to prevent collisions with context keys defined
// in other packages.
type ctxkey int

const (
	ridKey ctxkey = 0
	sidKey ctxkey = 1
)

// RIDFromContext extracts the request ID from ctx, if present.
func RIDFromContext(ctx context.Context) (rid uint64, ok bool) {
	rid, ok = ctx.Value(ridKey).(uint64)
	return
}

// SIDFromContext extracts the session ID from ctx, if present.
func SIDFromContext(ctx context.Context) (sid string, ok bool) {
	sid, ok = ctx.Value(sidKey).(string)
	return
}
