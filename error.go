// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"encoding/xml"
	"net/http"
)

var (
	ErrInvalidAck        = &ProtocolError{"invalid or missing ack"}
	ErrInvalidHold       = &ProtocolError{"invalid or missing hold attribute"}
	ErrInvalidInactivity = &ProtocolError{"invalid or missing inactivity period"}
	ErrInvalidMaxReqs    = &ProtocolError{"invalid or missing max requests num"}
	ErrInvalidPolling    = &ProtocolError{"invalid or missing polling interval"}
	ErrInvalidSID        = &ProtocolError{"invalid or missing session ID"}
	ErrInvalidWait       = &ProtocolError{"invalid or missing wait attribute"}
	ErrInvalidXML        = &ProtocolError{"bad XML data received"}
)

var (
	// BadRequest indicates that the format of an HTTP header or binding element
	// received from the client is unacceptable (e.g., syntax error).
	BadRequest BindingCondition = terminalCondition("bad-request")

	// HostGone indicates that the target domain specified in the 'to' attribute
	// or the target host or port specified in the 'route' attribute is no longer
	// serviced by the connection manager.
	HostGone BindingCondition = terminalCondition("hots-gone")

	// HostUnknown indicates that the target domain specified in the 'to'
	// attribute or the target host or port specified in the 'route' attribute is
	// unknown to the connection manager.
	HostUnknown BindingCondition = terminalCondition("host-unknown")

	// ImproperAddressing indicates that the initialization element lacks a 'to'
	// or 'route' attribute (or the attribute has no value) but the connection
	// manager requires one.
	ImproperAddressing BindingCondition = terminalCondition("improper-addressing")

	// InternalServerError indicates that the connection manager has experienced
	// an internal error that prevents it from servicing the request.
	InternalServerError BindingCondition = terminalCondition("internal-server-error")

	// ItemNotFound indicates that (1) 'sid' is not valid, (2) 'stream' is not
	// valid, (3) 'rid' is larger than the upper limit of the expected window, (4)
	// connection manager is unable to resend response, or (5) 'key' sequence is
	// invalid.
	ItemNotFound BindingCondition = terminalCondition("item-not-found")

	// OtherRequest indicates that another request being processed at the same
	// time as this request caused the session to terminate.
	OtherRequest BindingCondition = terminalCondition("other-request")

	// PolicyViolation indicates that the client has broken the session rules
	// (polling too frequently, requesting too frequently, sending too many
	// simultaneous requests).
	PolicyViolation BindingCondition = terminalCondition("policy-violation")

	// RemoteConnectionFailed indicates that the connection manager was unable to
	// connect to, or unable to connect securely to, or has lost its connection
	// to, the server.
	RemoteConnectionFailed BindingCondition = terminalCondition("remote-connection-failed")

	// RemoteStreamError encapsulates an error in the protocol being transported.
	RemoteStreamError BindingCondition = terminalCondition("remote-stream-error")

	// SystemShutdown indicates that the connection manager is being shut down.
	// All active HTTP sessions are being terminated. No new sessions can be
	// created.
	SystemShutdown BindingCondition = terminalCondition("system-shutdown")
)

// UndefinedCondition returns an error that acts as an application-level
// terminal condition. The innererr is marshaled into the undefined-condition
// error's inner XML or as char data if a string, byte array, or error is
// provided.
func UndefinedCondition(innererr interface{}) BindingCondition {
	bc := terminalCondition("undefined-condition")
	switch e := innererr.(type) {
	case error:
		bc.CharData = []byte(e.Error())
	case string:
		bc.CharData = []byte(e)
	case []byte:
		bc.CharData = e
	default:
		innerxml, _ := xml.Marshal(innererr)
		bc.InnerXML = innerxml
	}

	return bc
}

// SeeOtherURI returns an error that indicates that the connection manager does
// not operate at this URI (e.g., the connection manager accepts only SSL or TLS
// connections at some https: URI rather than the http: URI requested by the
// client). The provided URI will be set as the contents of the <uri/> child
// element.
func SeeOtherURI(uri string) BindingCondition {
	bc := terminalCondition("see-other-uri")
	innerxml, _ := xml.Marshal(struct {
		XMLName  xml.Name `xml:"uri"`
		CharData string   `xml:",chardata"`
	}{
		XMLName:  xml.Name{Space: "", Local: "uri"},
		CharData: uri,
	})
	bc.InnerXML = innerxml

	return bc
}

func terminalCondition(condition string) BindingCondition {
	return BindingCondition{
		XMLName:   bodyName,
		Condition: condition,
		Type:      "terminate",
		InnerXML:  []byte{},
	}
}

// BindingCondition is an error that represents a terminal or remote binding
// condition and is marshalable and unmarshalable from XML.
type BindingCondition struct {
	XMLName   xml.Name `xml:"http://jabber.org/protocol/httpbind body"`
	Condition string   `xml:"condition,attr,omitempty"`
	Type      string   `xml:"type,attr"`
	InnerXML  []byte   `xml:",innerxml"`
	CharData  []byte   `xml:",chardata"`
}

// Error satisfies the error interface by returning the name of the binding
// condition (eg. "policy-violation").
func (bc BindingCondition) Error() string {
	return bc.Condition
}

// legacyCondition gets a legacy HTTP error condition corresponding to the
// binding condition. For example, "policy-violation" returns 403 (FORBIDDEN).
// If no legacy error code exists for the given condition, return 200 (OK).
func legacyCondition(err error) int {
	switch bc := err.(type) {
	default:
		return http.StatusOK
	case BindingCondition:
		switch bc.Condition {
		case "bad-request":
			return http.StatusBadRequest
		case "policy-violation":
			return http.StatusForbidden
		case "item-not-found":
			return http.StatusNotFound
		default:
			return http.StatusOK
		}
	}
}

// DialError is an error that occurs while dialling a BOSH server.
type DialError struct {
	*Config
	ErrString string
}

// Error satisfies the error interface.
func (e *DialError) Error() string { return e.ErrString }

// ProtocolError represents unrecoverable errors due to unexpected requests or
// responses in the BOSH protocol such as badly formatted XML.
type ProtocolError struct {
	ErrorString string
}

// Error satisfies the error interface.
func (err *ProtocolError) Error() string { return err.ErrorString }
