module mellium.im/bosh

go 1.19

require (
	golang.org/x/net v0.0.0-20220812174116-3211cb980234
	golang.org/x/text v0.3.7
)
