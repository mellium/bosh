// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"encoding/xml"
	"io"
	"net/http"
	"net/url"

	"golang.org/x/text/language"
)

const (
	SupportedProtocolVersion = "1.11"
	DefaultWaitTime          = 5 // Seconds
)

const (
	HTTPBind      = "http://jabber.org/protocol/httpbind"
	XBOSH         = "urn:xmpp:xbosh"
	JabberStreams = "http://etherx.jabber.org/streams"
)

var (
	// bodyName is the full XML name of the BOSH <body/> element and is provided
	// as a convenience.
	bodyName = xml.Name{Space: HTTPBind, Local: "body"}
)

// Addr is an implementation of net.Addr for BOSH.
type Addr struct {
	*url.URL
}

// Network returns the network type for BOSH, "bosh".
func (addr *Addr) Network() string { return "bosh" }

// String returns a URL string for the address.
func (addr *Addr) String() string { return addr.URL.String() }

// Config is a BOSH configuration. Normally, the NewConfig function should be
// used for creating new configurations.
type Config struct {
	// BOSH protocol version
	Version string

	// A BOSH server address.
	Location *url.URL

	// A BOSH client origin.
	Origin *url.URL

	// Ack indicates that the client will be using acknowledgements throughout the
	// session.
	Ack bool

	// Wait specifies the longest time (in seconds) that the connection manager is
	// allowed to wait before responding to any request during the session. If it
	// is set on a server connection, it is the maximum wait time that a client
	// may request. This enables the client to limit the delay before it discovers
	// any network failure, and to prevent its HTTP/TCP connection from expiring
	// due to inactivity.
	Wait int

	// To specifies the target domain of the first stream; note that it is the
	// domain being served, not the hostname of the machine that is serving the
	// domain.
	To string

	// From specifies the originator of the first stream which enables the
	// connection manager to forward the originating entity's identity to the
	// application server (e.g., the JabberID of an entity that is connecting to
	// an XMPP server).
	From string

	// Header contains additional header fields to be sent with each HTTP request
	// or response.
	Header http.Header

	// Hold specifies the maximum number of requests the connection manager is
	// allowed to keep waiting at any one time during the session. If the client
	// is able to reuse connections, this value SHOULD be set to "1" (the default
	// if the NewConfig function is used to create this config).
	Hold int

	// Content is the value of the Content-Type header that should be returned for
	// every request by the server. Defaults to "text/xml; charset=utf-8". This
	// should probably never be modified.
	Content string

	// Lang specifies the default language of any human-readable XML character
	// data sent or received during the session.
	Lang language.Tag

	// Route is an optional value that specifies the protocol, hostname, and port
	// of the server with which we want to communicate, formatted as
	// "proto:host:port" (e.g., "xmpp:example.com:9999"). It is used to route
	// traffic to a specific host when connecting to proxy BOSH servers (or acting
	// as one) that serve multiple domains. Route may be ignored.
	Route string

	// Requests enables the connection manager to limit the number of simultaneous
	// requests the client makes. This value must be larger than the 'hold'
	// attribute value specified in the session request. Clients wlil keep open at
	// most this number of requests (if not set, defaults to Hold + 1) or whatever
	// the server specifies, whichever is smaller.
	Requests int

	// Inactivity specifies the longest allowable inactivity period (in seconds).
	// This value is only applicable to server configs.
	Inactivity int
}

// NewConfig creates a new BOSH config for a client connection.
func NewConfig(server, origin string) (config *Config, err error) {
	config = new(Config)
	config.Location, err = url.ParseRequestURI(server)
	if err != nil {
		return
	}
	config.Origin, err = url.ParseRequestURI(origin)
	if err != nil {
		return
	}
	config.Header = http.Header(make(map[string][]string))
	config.Hold = 1
	config.Wait = DefaultWaitTime
	config.Version = SupportedProtocolVersion
	return
}

type readWriteNopCloser struct {
	io.ReadWriter
}

func (rwc readWriteNopCloser) Read(p []byte) (n int, err error) {
	return rwc.ReadWriter.Read(p)
}
func (rwc readWriteNopCloser) Write(p []byte) (n int, err error) {
	return rwc.ReadWriter.Write(p)
}

func (readWriteNopCloser) Close() error { return nil }
