// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

// Package bosh is an implementation of XEP-0124: Bidirectional-streams Over
// Synchronous HTTP (BOSH).
//
// For more information see the spec:
// https://xmpp.org/extensions/xep-0124.html
//
// Be advised: This API is still unstable and is subject to change.
package bosh // import "mellium.im/bosh"
