// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

//go:build ignore

package bosh

import (
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
)

// Codec represents a symmetric pair of functions that implement a codec for
// marshaling and unmarshaling data from a BOSH connection.
type Codec struct {
	Marshal   func(v interface{}) (data []byte, err error)
	Unmarshal func(data []byte, v interface{}) (err error)
}

// Receive receives single XML element from conn, unmarshales it with
// cd.Unmarshal and stores it in v.
func (cd Codec) Receive(conn *Conn, v interface{}) (err error) {
	data, err := ioutil.ReadAll(conn)
	if err != nil {
		return err
	}
	return cd.Unmarshal(data, v)
}

// Send sends v marshaled by cd.Marshal in a single BOSH HTTP request.
func (cd Codec) Send(conn *Conn, v interface{}) (err error) {
	data, err := cd.Marshal(v)
	if err != nil {
		return err
	}
	_, err = conn.Write(data)
	return err
}

func jsonMarshal(v interface{}) (b []byte, err error) {
	j, err = json.Marshal(v)
	if err != nil {
		return b, err
	}
	b, err = xml.Marshal(struct {
		XMLName xml.Name `xml:"http://json.org/ json"`
		Data    []byte   `xml:",chardata"`
	}{
		Data: j,
	})
	return b, err
}

// JSON is a codec to send/receive JSON data over a BOSH connection. It wraps
// all data in a <json/> element qualified by the "http://json.org/" namespace.
var JSON = Codec{
	json.Marshal,
	json.Unmarshal,
}
