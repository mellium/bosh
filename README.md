# Go BOSH

[![GoDoc](https://godoc.org/mellium.im/bosh?status.svg)](https://godoc.org/mellium.im/bosh)
[![License](https://img.shields.io/badge/license-FreeBSD-blue.svg)](https://opensource.org/licenses/BSD-2-Clause)

A [BOSH][bosh] connection manager and client in Go.

## License

The package may be used under the terms of the BSD 2-Clause License a copy of
which may be found in the file [LICENSE.md][LICENSE].

[bosh]: https://xmpp.org/extensions/xep-0124.html
[LICENSE]: ./LICENSE.md
