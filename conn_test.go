// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"context"
	"net"
	"net/http"
	"net/url"
	"testing"
	"time"
)

var _ net.Conn = (*Conn)(nil)

func TestDefaultConnIsServerConn(t *testing.T) {
	c := &Conn{}
	if !c.IsServerConn() || c.IsClientConn() {
		t.Error("Conn should be a server conn by default")
	}
}

func TestPostToServerConnPanics(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Error("Calling post() on a server conn should panic")
		}
	}()

	c := &Conn{}
	c.post()
}

func TestSetDeadline(t *testing.T) {
	c := &Conn{config: &Config{}}
	c.SetDeadline(time.Now().Add(-1 * time.Hour))
	if n, err := c.Read([]byte{}); n != 0 || err == nil {
		t.Error("Deadline in the past should cause read failure")
	}
	if n, err := c.Write([]byte{0x01, 0x02}); n != 0 || err == nil {
		t.Error("Deadline in the past should cause write failure")
	}
}

func TestSetReadDeadline(t *testing.T) {
	c := &Conn{config: &Config{}}
	c.SetReadDeadline(time.Now().Add(-1 * time.Hour))
	if n, err := c.Read([]byte{}); n != 0 || err == nil {
		t.Error("Read deadline in the past should cause read failure")
	}
}

func TestSetWriteDeadline(t *testing.T) {
	c := &Conn{config: &Config{}}
	c.SetWriteDeadline(time.Now().Add(-1 * time.Hour))
	if n, err := c.Write([]byte{0x01, 0x02}); n != 0 || err == nil {
		t.Error("Write deadline in the past should cause write failure")
	}
}

func TestCloseConnCancelsCtx(t *testing.T) {
	c := &Conn{}
	c.ctx, c.cancel = context.WithCancel(context.Background())
	if _ = c.Close(); c.ctx.Err() != context.Canceled {
		t.Error("Closing a connection should cancel its context")
	}
}

func TestCloseOnClosedConnDoesNotPanic(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("Calling conn.Close on a closed connection should not panic")
		}
	}()

	c := &Conn{}
	c.ctx, c.cancel = context.WithCancel(context.Background())
	c.cancel()
	c.Close()
}

func TestLocalRemoteAddr(t *testing.T) {
	// Setup
	origin, err := url.Parse("https://xmpp.org/extensions/xep-0124.html")
	if err != nil {
		panic("You broke TestLocalAddr's origin URL")
	}
	location, err := url.Parse("https://xmpp.org/extensions/xep-0206.html")
	if err != nil {
		panic("You broke TestLocalAddr's location URL")
	}
	if origin.String() == location.String() {
		panic("You broke TestLocalAddr: origin and location should be different strings")
	}

	// Test Server Conn
	c := &Conn{config: &Config{Origin: origin, Location: location}}
	if localaddr, ok := c.LocalAddr().(*Addr); !ok || localaddr.String() != location.String() {
		t.Error("Server conn LocalAddr returned the wrong value")
	}
	if remoteaddr, ok := c.RemoteAddr().(*Addr); !ok || remoteaddr.String() != origin.String() {
		t.Error("Server conn RemoteAddr returned the wrong value")
	}

	// Test Client Conn
	c = &Conn{config: &Config{Origin: origin, Location: location}, client: &http.Client{}}
	if localaddr, ok := c.LocalAddr().(*Addr); !ok || localaddr.String() != origin.String() {
		t.Error("Client conn LocalAddr returned the wrong value")
	}
	if remoteaddr, ok := c.RemoteAddr().(*Addr); !ok || remoteaddr.String() != location.String() {
		t.Error("Client conn RemoteAddr returned the wrong value")
	}
}
