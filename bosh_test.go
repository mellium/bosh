// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"fmt"
	"net"
)

var _ net.Addr = (*Addr)(nil)
var _ fmt.Stringer = (*Addr)(nil)
