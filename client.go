// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"bytes"
	"context"
	"encoding/xml"
	"net/http"
	"time"

	"mellium.im/bosh/internal"
)

// NewClient creates a new BOSH client connection using ctx as the base context
// for all requests made by the client.
func NewClient(ctx context.Context, config *Config) (*Conn, error) {
	conn := &Conn{
		config:      config,
		client:      &http.Client{},
		ctx:         ctx,
		readbuf:     &bytes.Buffer{},
		writebuf:    &bytes.Buffer{},
		lastSeenRID: internal.NewRID(),
	}

	ssr := internal.StartSessionRequest{
		Content: conn.config.Content,
		Lang:    conn.config.Lang.String(),
		Route:   conn.config.Route,
	}
	ssr.Hold = conn.config.Hold
	ssr.To = conn.config.To
	ssr.Ver = config.Version
	ssr.Wait = conn.config.Wait
	ssr.From = conn.config.From
	ssr.RID = conn.lastSeenRID

	if config.Ack {
		ssr.Ack = 1
	} else {
		ssr.Ack = 0
	}

	// Nothing should have references to this yet, but aquire the mutex
	// defensively like a good citizen.
	conn.wio.Lock()
	defer conn.wio.Unlock()
	conn.rio.Lock()
	defer conn.rio.Unlock()

	e := xml.NewEncoder(conn.writebuf)
	if err := e.Encode(ssr); err != nil {
		return nil, err
	}
	if err := conn.post(); err != nil {
		return nil, err
	}
	d := xml.NewDecoder(conn.readbuf)
	t, err := d.Token()
	if err != nil {
		return nil, err
	}

	se, ok := t.(xml.StartElement)
	if !ok {
		return nil, ErrInvalidXML
	}
	resp := internal.StartSessionResponse{}
	if err := d.DecodeElement(&resp, &se); err != nil {
		return nil, err
	}

	// Do some validation on the response.
	switch {
	case resp.SID == "":
		return nil, ErrInvalidSID
	case resp.Wait < 0 || resp.Wait > config.Wait:
		return nil, ErrInvalidWait
	case resp.Polling < 0:
		return nil, ErrInvalidPolling
	case resp.Inactivity < 0:
		return nil, ErrInvalidInactivity
	case resp.Hold < 0 || resp.Hold > config.Hold:
		return nil, ErrInvalidHold
	case resp.Ack != 0 && resp.Ack != conn.lastSeenRID:
		return nil, ErrInvalidAck
	}

	switch {
	case resp.Requests <= config.Hold:
		return nil, ErrInvalidMaxReqs
	case resp.Requests < config.Requests:
		conn.requests = resp.Requests
	case config.Requests == 0:
		conn.requests = resp.Hold + 1
	default:
		conn.requests = config.Requests
	}

	conn.sid = resp.SID
	conn.wait = time.Duration(resp.Wait) * time.Second
	conn.hold = resp.Hold
	conn.polling = time.Duration(resp.Polling) * time.Second
	conn.inactivity = time.Duration(resp.Inactivity) * time.Second
	conn.to = resp.To
	conn.ack = resp.Ack
	conn.maxpause = time.Duration(resp.MaxPause) * time.Second
	conn.from = resp.From

	go conn.manageClient()

	return conn, nil
}

// Dial opens a new client connection to a BOSH server and starts a new session.
// The connection has an in-memory buffer that is flushed as new HTTP requests
// are made.
func Dial(url, origin string) (conn *Conn, err error) {
	config, err := NewConfig(url, origin)
	if err != nil {
		return nil, err
	}
	return DialConfig(config)
}

// DialConfig opens a new BOSH client connection with a config.
func DialConfig(config *Config) (conn *Conn, err error) {
	switch {
	case config.Location == nil:
		return nil, &DialError{config, "Missing or bad BOSH location"}
	case config.Origin == nil:
		return nil, &DialError{config, "Missing or bad BOSH origin"}
	case config.Location.Scheme != "http" && config.Location.Scheme != "https":
		return nil, &DialError{config, "Bad scheme: " + config.Location.Scheme}
	}

	return NewClient(context.Background(), config)
}
