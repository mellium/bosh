// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"context"
	"net"
	"net/http"
	"sync"

	"golang.org/x/net/trace"
)

// BUG(ssw): Server functionality is an API definition and doesn't work yet.

// Handler is a simple http.Handler interface to BOSH.
type Handler func(*Conn)

// ServeHTTP implements the http.Handler interface for BOSH.
func (h Handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	s := Server{Handler: h}
	s.serveBOSH(w, req)
}

// Server is a BOSH session manager.
type Server struct {
	Config

	// Dial will be used when making new upstream connections.
	//
	// If Dial is nil, net.Dial is used.
	Dial func(network, addr string) (net.Conn, error)

	// Handler defines the behavior of the server. It can be used to handle
	// subprotocols such as XMPP, to proxy a BOSH connection to a different
	// protocol, etc.
	Handler

	// EventLog facilitates logging to the trace web interface, or to any
	// log.Logger that implements the EventLog interface.
	EventLog trace.EventLog

	m        sync.Mutex
	sessions map[string]*Conn
	ctx      context.Context
}

// Creates a new server that will use the given context as the base context for
// all requests. When the context is closed the server will attempt to shut down
// cleanly.
func NewServer(ctx context.Context) *Server {
	return &Server{ctx: ctx}
}

// ServeHTTP satisfies the http.Handler interface.
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.serveBOSH(w, r)
}

func (s *Server) serveBOSH(w http.ResponseWriter, req *http.Request) {
	s.m.Lock()
	defer s.m.Unlock()
	if s.sessions == nil {
		s.EventLog.Printf("Initializing session storage")
		s.sessions = make(map[string]*Conn)
	}
	panic("Not yet implemented")
}
