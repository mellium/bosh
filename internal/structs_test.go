// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package internal

import (
	"encoding/xml"
	"testing"
)

func TestMarshalStartSessionResponse(t *testing.T) {
	b, _ := xml.Marshal(StartSessionResponse{})
	s := `<body xmlns="http://jabber.org/protocol/httpbind" sid="" wait="0" requests="0"></body>`
	if body := string(b); s != body {
		t.Logf("Failed to marshal session body:\ngot:\n%v\n want:\n%v\n", body, s)
		t.Fail()
	}
}
