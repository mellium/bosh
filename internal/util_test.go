// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package internal

import (
	"errors"
	"testing"
)

type zeroReader struct{}

func (zeroReader) Read(p []byte) (n int, err error) {
	n = cap(p)
	for i := 0; i < n; i++ {
		p[i] = 0
	}
	return
}

type errReader struct{}

func (errReader) Read(p []byte) (n int, err error) {
	err = errors.New("Got expected error when attempting to read from errReader")
	return
}

func TestNewRIDNeverZero(t *testing.T) {
	r := newRID(zeroReader{})
	if r != 1 {
		t.Logf("Unexpected value for new RID: want=1, got=%v", r)
		t.Fail()
	}
}

func TestNewRIDPanicsOnError(t *testing.T) {
	defer func() {
		recover()
	}()

	newRID(errReader{})

	t.Logf("Expected NewRID to panic on read error.")
	t.Fail()
}

func TestNewSIDReadsCorrectLength(t *testing.T) {
	n := 0
	newSID(func(b []byte) (int, error) {
		n = cap(b)
		return n, nil
	})
	if n != sidBytes {
		t.Logf("NewSID read wrong number of bytes: want=%v, got=%v", sidBytes, n)
		t.Fail()
	}
}

func TestNewSIDPanicsOnError(t *testing.T) {
	defer func() {
		recover()
	}()

	newSID(func(b []byte) (n int, err error) {
		return 0, errors.New("Got expected error when calling err read func")
	})

	t.Logf("Expected NewSID to panic on read error.")
	t.Fail()
}
