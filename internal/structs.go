// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package internal

import (
	"encoding/xml"
)

// body represents a BOSH <body/> element with the correct namespace.
type Body struct {
	XMLName xml.Name `xml:"http://jabber.org/protocol/httpbind body"`

	From     string `xml:"from,attr,omitempty"`
	InnerXML []byte `xml:",innerxml"`
	Type     string `xml:"type,attr,omitempty"`
}

// SessionBody contains shared attriutes that are used by session start requests
// and the initial response.
type SessionBody struct {
	Body

	Ack  uint64 `xml:"ack,attr,omitempty"`
	Hold int    `xml:"hold,attr,omitempty"`
	To   string `xml:"to,attr,omitempty"`
	Ver  string `xml:"ver,attr,omitempty"`
	Wait int    `xml:"wait,attr"`
}

// A StartSessionRequest is used to unmarshal a new session request without a
// payload.
type StartSessionRequest struct {
	Body
	RequestInfo
	SessionBody

	Content string `xml:"content,attr,omitempty"`
	Lang    string `xml:"http://www.w3.org/XML/1998/namespace lang,attr"`
	Route   string `xml:"route,attr"`
}

// SessionInfo contains metadata about the current session.
type SessionInfo struct {
	SID string `xml:"sid,attr"`
}

// RequestInfo contains metadata about the current request.
type RequestInfo struct {
	RID uint64 `xml:"rid,attr"`
}

// A RequestPayload is any request that is part of an existing session.
type RequestPayload struct {
	Body
	SessionInfo
	RequestInfo
}

// StartSessionResponse is used to marshal a response to the initial session
// start request.
type StartSessionResponse struct {
	Body
	SessionInfo
	SessionBody

	// Body MUST possess the following attributes:
	Requests int `xml:"requests,attr"`

	// Body SHOULD possess the following attributes:
	Inactivity int `xml:"inactivity,attr,omitempty"`
	Polling    int `xml:"polling,attr,omitempty"`

	// Body MAY possess the following attributes:
	Accept   string `xml:"accept,attr,omitempty"`
	MaxPause int    `xml:"maxpause,attr,omitempty"`
	CharSets string `xml:"charsets,attr,omitempty"`
}
