// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package internal

import (
	"container/heap"
	"testing"
)

var _ heap.Interface = (*PayloadQueue)(nil)

func TestPayloadQueue_Peek(t *testing.T) {
	pq := make(PayloadQueue, 0)
	heap.Push(&pq, &Payload{
		Value: nil,
		RID:   10,
	})
	heap.Push(&pq, &Payload{
		Value: nil,
		RID:   5,
	})
	heap.Init(&pq)

	pl := pq.Peek()
	if pl.RID != 5 {
		t.Logf("Peek returned wrong RID: want=5, got=%v", pl.RID)
		t.Fail()
	}
	if len(pq) != 2 {
		t.Logf("Queue is the wrong size after Peek: want=2, got=%v", len(pq))
		t.Fail()
	}
}
