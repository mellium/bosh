// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//
// This example was copied and modified from the example in containers/heap

package internal_test

import (
	"container/heap"
	"fmt"

	"mellium.im/bosh/internal"
)

// This example creates a PayloadQueue with some payloads, adds and manipulates
// a payload, and then removes the payloads in reverse priority order.
func ExamplePayloadQueue() {
	// Some payloads and their priorities.
	payloads := map[string]uint64{
		"banana": 3, "apple": 2, "pear": 4,
	}

	// Create a reverse priority queue, put the payloads in it, and establish the
	// reverse priority queue (heap) invariants.
	pq := make(internal.PayloadQueue, len(payloads))
	i := 0
	for value, priority := range payloads {
		pq[i] = &internal.Payload{
			Value: value,
			RID:   priority,
		}
		i++
	}
	heap.Init(&pq)

	heap.Push(&pq, &internal.Payload{
		Value: "orange",
		RID:   5,
	})

	// Take the payloads out; they arrive in increasing priority order.
	for pq.Len() > 0 {
		payload := heap.Pop(&pq).(*internal.Payload)
		fmt.Printf("%.2d:%s ", payload.RID, payload.Value)
	}
	// Output:
	// 02:apple 03:banana 04:pear 05:orange
}
