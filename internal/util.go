// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package internal

import (
	"crypto/rand"
	"encoding/hex"
	"io"
	"math/big"
)

const (
	// MaxInitialRID is the largest possible starting request ID.
	MaxInitialRID = uint64(float64(9007199254740991) * 0.75)
)

const (
	// sidBytes is the number of bytes of random data in a session ID
	// (len(sid)/2).
	sidBytes = 16
)

// NewRID returns a new cryptographically secure random ID that is low enough to
// prevent any reasonable session from rolling over the maximum safe number for
// a double-precision floating-point value (2⁵³-1). If a proper source of
// entropy can't be initialized (or generating a random number fails for any
// other reason), the function panics. RID will not be 0. The range will be (0,
// MaxInitialRID]
func NewRID() uint64 {
	return newRID(rand.Reader)
}

var maxInitialRID *big.Int = new(big.Int).SetUint64(MaxInitialRID - 1)

func newRID(r io.Reader) uint64 {
	i, err := rand.Int(r, maxInitialRID)
	if err != nil {
		panic(err.Error())
	}
	return i.Uint64() + 1 // Don't allow 0…
}

// NewSID returns a new cryptographically secure random session ID. If for any
// reason generating randomness fails, the function panics.
func NewSID() string {
	return newSID(rand.Read)
}

func newSID(f func([]byte) (int, error)) string {
	b := make([]byte, sidBytes)
	_, err := f(b)
	if err != nil {
		panic(err.Error())
	}

	return hex.EncodeToString(b)
}
