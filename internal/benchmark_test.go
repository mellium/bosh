// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package internal

import (
	"container/heap"
	"testing"
)

func BenchmarkHeapInsert100Sequential(b *testing.B) {
	pq := &PayloadQueue{}
	heap.Init(pq)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for j := 0; j < 100; j++ {
			heap.Push(pq, &Payload{
				Value: "payload",
				RID:   uint64(j),
			})
		}
	}
}
