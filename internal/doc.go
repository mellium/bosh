// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

// Package internal provides internal APIs used by the bosh package.
package internal // import "mellium.im/bosh/internal"
