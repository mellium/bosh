// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//
// This file was copied and modified from the example in containers/heap

package internal

type Payload struct {
	Value interface{}
	RID   uint64

	Index int
}

type PayloadQueue []*Payload

func (pq PayloadQueue) Len() int { return len(pq) }

func (pq PayloadQueue) Less(i, j int) bool {
	// We want Pop to give us the lowest rid.
	return pq[i].RID < pq[j].RID
}

func (pq PayloadQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].Index = i
	pq[j].Index = j
}

func (pq *PayloadQueue) Push(x interface{}) {
	n := len(*pq)
	payload := x.(*Payload)
	payload.Index = n
	*pq = append(*pq, payload)
}

func (pq *PayloadQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	payload := old[n-1]
	payload.Index = -1 // for safety
	*pq = old[0 : n-1]
	return payload
}

// Peek returns a copy of the last element on the heap (without removing it).
func (pq *PayloadQueue) Peek() *Payload {
	return (*pq)[0]
}
