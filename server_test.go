// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"net/http"
)

var _ http.Handler = (*Server)(nil)
var _ http.Handler = (*Handler)(nil)
