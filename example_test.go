// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"bytes"
	"encoding/xml"
	"fmt"
)

func ExampleBindingCondition_marshal() {
	b, _ := xml.Marshal(UndefinedCondition("The server is dead, Jim."))
	fmt.Println(string(b))
	b, _ = xml.Marshal(SeeOtherURI("https://likeabosh.hipchat.com/http-bind"))
	fmt.Println(string(b))
	// Output:
	// <body xmlns="http://jabber.org/protocol/httpbind" condition="undefined-condition" type="terminate">The server is dead, Jim.</body>
	// <body xmlns="http://jabber.org/protocol/httpbind" condition="see-other-uri" type="terminate"><uri>https://likeabosh.hipchat.com/http-bind</uri></body>
}

func ExampleBindingCondition_unmarshal() {
	e := []byte(`<body condition='see-other-uri'
                     type='terminate'
                     xmlns='http://jabber.org/protocol/httpbind'>
    <uri>https://likeabosh.hipchat.com/http-bind</uri>
</body>
`)
	bc := BindingCondition{}
	xml.Unmarshal(e, &bc)
	fmt.Printf("%s: %s", bc.Error(), bytes.TrimSpace(bc.InnerXML))
	// Output:
	// see-other-uri: <uri>https://likeabosh.hipchat.com/http-bind</uri>
}
