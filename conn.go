// Copyright 2016 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package bosh

import (
	"bytes"
	"context"
	"errors"
	"net"
	"net/http"
	"sync"
	"time"
)

var (
	errTimeout = errors.New("bosh: Operation timed out")
)

// BUG: Currently the read/write buffers can expand indefinitely.

// Conn represents a BOSH connection.
//
// Multiple goroutines may invoke methods on a Conn simultaneously.
type Conn struct {
	config *Config

	rio          sync.Mutex
	readbuf      *bytes.Buffer
	readDeadline time.Time

	wio           sync.Mutex
	writebuf      *bytes.Buffer
	writeDeadline time.Time

	client *http.Client

	lastSeenRID uint64

	ctx    context.Context
	cancel context.CancelFunc

	// Some of these attributes duplicate what's in Config, but I don't think we
	// should modify config after negotiating the final values to prevent
	// confusion and race conditions where the user uses one config for multiple
	// connections. We also can't just copy config because it has other pointers.
	sid        string
	wait       time.Duration
	polling    time.Duration
	inactivity time.Duration
	requests   int
	to         string
	from       string
	ack        uint64
	maxpause   time.Duration
	hold       int
}

// manageClient handles making the async HTTP requests that make up a BOSH
// client connection and is called in a goroutine by NewClient after the initial
// connection setup.
func (conn *Conn) manageClient() {
	for {
		select {
		case <-conn.ctx.Done():
			conn.Close()
			return
		default:
			// HTTP requests
		}
	}
}

// post sends a post request with the contents of the write buffer and copies
// the response body into the read buffer. This gets called a lot, so be careful
// not to put any difficult work in here.
//
// If post is called on a server connection, it panics.
func (conn *Conn) post() error {
	if conn.IsServerConn() {
		panic("bosh: Tried to POST to a server connection")
	}

	// TODO: Is this the idiomatic way to make sure we unlock the mutex as soon as
	// the post is done, but ensure it gets unlocked even if the Post or buffer
	// expansion panics?
	// TODO: Is the extra stack frame allocation here too much overhead?
	resp, err := func() (*http.Response, error) {
		conn.wio.Lock()
		defer conn.wio.Unlock()
		req, err := http.NewRequest("POST", conn.config.Location.String(), conn.writebuf)
		if err != nil {
			return nil, err
		}
		req.Header.Set("Content-Type", "application/xml")
		req.Header = conn.Config().Header

		return conn.client.Do(req.WithContext(conn.ctx))
	}()
	if err != nil {
		return err
	}

	conn.rio.Lock()
	defer conn.rio.Unlock()
	if _, err := conn.readbuf.ReadFrom(resp.Body); err != nil {
		conn.Close()
		return err
	}
	return resp.Body.Close()
}

// Config returns the BOSH config.
func (conn *Conn) Config() *Config {
	return conn.config
}

// Read satisfies the io.Reader interface: It reads a single response from the
// BOSH connection and populates b with the contents of the <body/> element.
func (conn *Conn) Read(b []byte) (int, error) {
	conn.rio.Lock()
	defer conn.rio.Unlock()
	if !conn.readDeadline.IsZero() && time.Now().After(conn.readDeadline) {
		return 0, &net.OpError{Op: "read", Net: "tcp", Source: conn.LocalAddr(), Addr: conn.RemoteAddr(), Err: errTimeout}
	}
	return conn.readbuf.Read(b)
}

// Write satisfies the io.Writer interface: It writes the given bytes to the
// internal buffer to be sent in a later HTTP request or response.
func (conn *Conn) Write(b []byte) (int, error) {
	conn.wio.Lock()
	defer conn.wio.Unlock()
	if !conn.writeDeadline.IsZero() && time.Now().After(conn.writeDeadline) {
		return 0, &net.OpError{Op: "write", Net: "tcp", Source: conn.LocalAddr(), Addr: conn.RemoteAddr(), Err: errTimeout}
	}
	return conn.writebuf.Write(b)
}

// Close implements the io.Closer interface.
func (conn *Conn) Close() error {
	if conn.cancel != nil {
		// TODO: If the conn is already closed what's the expected behavior? Return
		// an OpError?
		conn.cancel()
	}
	return nil
}

// IsClientConn is true if the BOSH connection is a client connection.
func (conn *Conn) IsClientConn() bool { return conn.client != nil }

// IsServerConn is true if the BOSH connection is a server connection.
func (conn *Conn) IsServerConn() bool { return conn.client == nil }

// LocalAddr returns Origin for a client connection, or Location for a server
// connection.
func (conn *Conn) LocalAddr() net.Addr {
	if conn.IsClientConn() {
		return &Addr{conn.config.Origin}
	}
	return &Addr{conn.config.Location}
}

// RemoteAddr returns Location for a client connection, or Origin for a server
// connection.
func (conn *Conn) RemoteAddr() net.Addr {
	if conn.IsClientConn() {
		return &Addr{conn.config.Location}
	}
	return &Addr{conn.config.Origin}
}

// SetDeadline sets the connection's network read & write deadlines.
func (conn *Conn) SetDeadline(t time.Time) error {
	conn.SetReadDeadline(t)
	conn.SetWriteDeadline(t)
	return nil
}

// SetReadDeadline sets the connection's network read deadline.
func (conn *Conn) SetReadDeadline(t time.Time) error {
	conn.readDeadline = t
	return nil
}

// SetWriteDeadline sets the connection's network write deadline.
func (conn *Conn) SetWriteDeadline(t time.Time) error {
	conn.writeDeadline = t
	return nil
}
